//Math.floor() in js is a function that returns the largest integer less than or equal to a given number.
//score is now being defined in move evaluation and is reflective of the valuation of the board

//Change variables player_score and cpu_score to aiPlayer and humanPlayer
//assign the player the corresponding value of its tokens
//This needs to be done in the integration process for the main game
//Explanatory placeholders used
//Also, need I need to deal with changing to tokens for players to integer rather than string


//-------------------Testing Variables----------------------------------------

let  gameBoard = [
  ['open', 'open', 'open', 'open', 'open', 'open'],
  ['open', 'open', 'open', 'open', 'open', 'open'],
  ['open', 'open', 'open', 'open', 'open', 'open'],
  ['open', 'open', 'open', 'open', 'open', 'open'],
  ['open', 'open', 'open', 'open', 'open', 'open'],
  ['open', 'open', 'open', 'open', 'open', 'open'],
  ['open', 'open', 'open', 'open', 'open', 'open']
];

document.getElementById('test').onclick = function(){
    test(gameBoard); // calls the test() method
}

function test(board) {
    // make the calls to your other functions inside of this
    // function. It should return an integer ideally once everything
    // is finished, but feel free to change what gets returned for testing
    // and debugging purposes!
    let response = 1 //michelle's function call here!
    console.log(response) //print whatever comes back in the console
}


//track available moves
let availableMoves = [];


// Initializing the players

let ai = 'red';
let human = 'green';
let currentPlayer = human;



//-----------------------End Test Variables--------------------------------------------------

//i is just the row value being placed passed into the call
function nextSpace(i) {
  let col = i;
  for (let j = 5; j >= 0; j--) {
    if (gameBoard[col][j] == 'empty') {
      return j;
    }//end if
  }//end for
}//end nextSpace

//Need to move
function aiMove() {
  let row = floor(random(1) * 7);

  gameBoard[row][nextSpace(row)] = aiPlayer;

//!!Add switch player to human command here
//player = humanPlayer or however it changes
}


function evaluateAI() {
  let bestMove = -99999;
  let move = [];

 //
  for (let i = 0; i < 7; i++) {
   //I am not sure if we need to start with let j = nextSpace(i) or what is there;
    for (let j = 0; j < 6; j++) {

      if (nextSpace(i) == j) {

      if (connectTwo(humanPlayer) == 0 && connectTwo(aiPlayer) == 0) {
        let col = floor(random(1) * 7);
        let row = nextSpace(col);

        move = [col, row];
      }
      else {
        gameBoard[i][nextSpace(i)] = aiPlayer;
        let score = alphaBeta(gameBoard, 5, -99999, 99999, false);

        gameBoard[i][nextSpace(i) + 1] = 'open';

        if (score > bestMove) {
          bestMove = score;
          move = [i,  j];
        }//if score is best move

      }//end else
      }//end if nextSpace
    }//end for j
  }//end for i

  gameBoard[move[0]][nextSpace(move[0])] = ai;

  //!!Add switch player to human command here
}//end evaluateAI



function gameOver() {

  let aiWins = connectFour(aiPlayer);
  let humanWins = connectFour(humanPlayer);


//find the open spaces
  let openSpace = 0;
  //iterate 7 columns
  for (let i = 0; i < 7; i++) {
  //iterate 6 rows
    for (let j = 0; j < 6; j++) {
      if (gameBoard[i][j] == 0) {
        openSpace++;
      }//end if
    }//end inner for j
  }//end outer for i

  if (aiWins > 0) {
    return aiPlayer;
  } //if ai
  else if (humanWins > 0) {
    return humanPlayer;
  } //if hum
  else if (openSpace == 0) {
    return 'tie';
  }//if tie

}//end gameOver function


//Pulled out as much commonality in the scanning of the board/board evaluations as I could
function checkFour(start, two, three, four) {
  return (start ==two  && two == three && three == four && (start != 'open'));
}

//Are there four in a row?
//player will feed in aiPlayer or humanPlayer -->the value should be the token representation of the player
function connectFour(player) {

  let fourCount = 0;

  //iterate through board
  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < 6; j++) {

          //Check for player (so we know what value/symbols to look for)
           if(gameBoard[i][j] == player){

            //a diaganol or horizontal is connect four is right side bound : it must start at row 3 or under (7-4)
            if(i < 4){

               //check for vertical connect four
               if (checkFour(gameBoard[i][j], gameBoard[i + 1][j], gameBoard[i + 2][j], gameBoard[i + 3][j])) {
                 fourCount++;
               }//end if vertical

               //check NW diagonal
               else if ((j < 3) && checkFour(gameBoard[i][j], gameBoard[i + 1][j + 1], gameBoard[i + 2][j + 2], gameBoard[i + 3][j + 3])){
                  fourCount++;
                }//end if NW diagonal
            }//end if right-bound

           //: it must start at column 2 or under (6-4)
           else if(j < 3){

           //check vertical
             if (checkFour((gameBoard[i][j], gameBoard[i][j + 1], gameBoard[i][j + 2], gameBoard[i][j + 3]))) {
              fourCount++;
            }//end if horizontal

             //check NE diagonal
             else if((i > 2) && checkFour(gameBoard[i][j], gameBoard[i - 1][j + 1], gameBoard[i - 2][j + 2], gameBoard[i - 3][j + 3])) {
              fourCount++;
            }//end  else if NE diagonal

         }//end else if top bound
      }//check for player
    }//end for j
  }//end for i

  return fourCount;
}


function checkThree(start, two, three){
  return (start == two &&  two == three && (start != 'open'));
}

//player will feed in aiPlayer or humanPlayer -->the value should be the token representation of the player
function connectThree(player) {
  let threeCount = 0;

  //iterate through board
  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < 6; j++) {

      if(gameBoard[i][j] == player){

         //a horizontal or diaganol connect three is right side: it must start at column 4 or under (7-3)
         if(i < 5){

            //check horizontal
            if(checkThree(gameBoard[i][j], gameBoard[i + 1][j], gameBoard[i + 2][j])) {
              threeCount++;
            } //end check

            //check NE diagonal
            else if ((j < 4) &&  checkThree(gameBoard[i][j], gameBoard[i + 1][j + 1], gameBoard[i + 2][j + 2])){
                       threeCount++;
            } //end NE diagonal
         }//end if right bound

         //a vertical or diaganol connect three is top-bound:  it must start at row 3 or under (6-3)
         //Yes, the one above fits (j-4), but this is how I broke it up.  Can change if needed
         else if (j < 4){

            //check vertical
            if (checkThree(gameBoard[i][j], gameBoard[i][j + 1], gameBoard[i][j + 2])) {
              threeCount++;
            } //end

            //check NW diagonal
            else if ((i > 1) &&  checkThree(gameBoard[i][j], gameBoard[i - 1][j + 1], gameBoard[i - 2][j + 2])) {
              threeCount++;
            }//end NW

         }//end top bound
       }//end if player
     }//end j
  }//end i

  return threeCount;

}

//Pulled out as much commonality in the scanning of the board/board evaluations as I could
function checkTwo(start, two) {
    return (start == two && (start != 'open'));
}

//Check for two in a row
function connectTwo(player){

  let twoCount = 0;

  //iterate board
  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < 6; j++) {

      if(gameBoard[i][j] == player){

         //check for horizontal
         if ((i < 6) &&  checkTwo(gameBoard[i][j], gameBoard[i + 1][j])) {
           twoCount++;
         } //end vertical

         //check right bounds
         else if (j < 5){

            //check NE vertical
            if(checkTwo(gameBoard[i][j], gameBoard[i][j + 1])) {
              twoCount++;
            } //end NE

            //check NE diagonal
            else if ((i < 6) &&checkTwo(gameBoard[i][j], gameBoard[i + 1][j + 1])) {
              twoCount++;
            }

            //check NW diagonal
            else if ((i > 0) && checkTwo(gameBoard[i][j], gameBoard[i - 1][j + 1]) ) {
              twoCount++;
            }//end

      }//end right bound
    }//end player
    }//end j
  }//end i

  return twoCount;

}//end connectTwo


//Need to set scores
let scores = {
  aiScore: +100000,
  humanScore: -100000,
  tie: 0
};

//get the heuristic score for pruning/minmax evaluation
function hScore() {

  let aiConnectFour = connectFour(aiPlayer);
  let humanConnectFour = connectFours(human);

  let aiConnectThrees = connectThree(aiPlayer) * 1000;
  let humanThrees = connectThrees(humanPlayer) * 1000;

  let aiConnectTwos = connectTwo(aiPlayer) * 10;
  let humanConnectTwos = connectTwos(humanPlayer) * 10;

  if (aiConnectFour > 0) {
    return 100000;
  }

  if (humanConnectFour > 0) {
    return -100000;
  }

  let hScore = aiConnectThrees + aiConnectTwos - humanThrees - humanConnectTwos;

  console.log('Test heuristic score function.  hScore = : ' + heuristicScore);
  return hScore;
}


//alphaBeta
function alphaBeta(board, depth, alpha, beta, isMax) {
  //are we still playing?
  let haveWinner = gameOver();

  if (haveWinner != null) {
    return scores[haveWinner];
  }

  if (depth == 0) {
    let evaluation = hScore();

    return evaluation;
  }

  if (isMax) {

    let bestScore = -99999;
    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 6; j++) {
        if (nextSpace(i) == j) {
          board[i][j] = aiPlayer;
          let score = alphaBeta(board, depth - 1, alpha, beta, false);
          board[i][j] = 'open';
          bestScore = max(score, bestScore);
          alpha = max(alpha, score);
          if (beta <= alpha) {
            break;
          }
        }
      }
    }
    return bestScore;
  }
  else {

    let bestScore = -99999;
    for (let i = 0; i < 7; i++) {
      for (let j = 0; j < 6; j++) {
        if (nextSpace(i) == j) {
          board[i][j] = humanPlayer;
          let score = alphaBeta(board, depth - 1, alpha, beta, true);
          board[i][j] = 'open';
          bestScore = min(score, bestScore);
          beta = min(beta, score);
          if (beta <= alpha) {
            break;
          }
        }
      }
    }
    return bestScore;
  }//end else
}//

//-------------------Testing Game----------------------------------------

let mouseMoved = false;

function mouseAction() {
  mouseMoved = true;
}

function mouseClicked() {

//humanPlayer moves
  if (currentPlayer == humanPlayer) {
    let temp = floor(abs(((mouse + 30) - 3 * verticalFrame) / (verticalFrame + circle)));

    board[temp][nextSpace(temp)] = humanPlayer;
    currentPlayer = aiPlayer;
    setTimeout(bestMove, 500);
  }

//Change the available moves
  for (let i = 0; i < availableMoves.length; i++) {
    if (available[i] !== '') {
      available.splice(i, 1);
    }
  }
}

//####Create Graphics
// Initializing drawing variables

let circle = 60;
let horizontalFrame = 20;
let verticalFrame = 20;


//reference: https://p5js.org/reference/#/p5/draw
// The statements in the setup() function
// execute once when the program begins
function setup() {
  // createCanvas must be the first statement
  //Creates a canvas element in the document, and sets the dimensions of it in pixels
  createCanvas(720, 400);
  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < 6; j++) {
      availableMoves.push(gameBoard[i][j]);
    }
  }
}

//draw() follows setup()
function draw() {

  // Board background drawing

  background(0, 150, 255);

  for (let i = 0; i < 7; i++) {
    for (let j = 0; j < 6; j++) {

      //Draw "board"

      let circleA = (i + 3) * horizontalFrame + ((2 * i)) * (circle/2);
      let circleB = (j + 4) * horizontalFrame + ((2 * j)) * (circle/2);

      fill(color(255, 255, 255));
      noStroke();
      ellipse(CircleA, CircleB, circle);

      // Pieces

      let placeToken = board[i][j];

      if (placeToken == aiPlayer) {
        fill(color(255, 0, 0));
        ellipse(CircleA, CircleB, circle);
      }
      else if (placeToken == humanPlayer) {
        fill(color(0,128,0));
        ellipse(CircleA, CircleB, circle);
      }
    }
  }

  if (mouseMoved) {
   let temp = floor(abs(((mouse + 30) - 3 * horizontalFrame) / (horizontalFrame + circle)));

   let temp2 = nextSpace(temp);

   noFill();
   stroke(color(255, 255, 0));
   strokeWeight(15);

   ellipse((temp + 3) * horizontalFrame + ((2 * temp)) * circle, (temp2 + 4) * verticalFrame + ((2 * temp2)) * circle, 60);
 }
 // Recording Result of Game

 let haveWinner = gameOver();
 if (haveWinner != null) {
   console.log(haveWinner);
   noLoop();
 }
}

//-----------------------End Testing Game--------------------------------------------------
