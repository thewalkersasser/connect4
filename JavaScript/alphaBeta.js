//Kennesaw State University
//College of Computing and Software Engineering 
//Computer Science Department 
//Algorithm Analysis
//CS 4306-01
//David Amanze, Walker Sasser, Michelle Campbell
//alphaBeta.js pruning portion for our Connect 4 javascript game


function maximizeMove(gameBoard, depth, alpha, beta) {
    // Call score of our board
    var score = gameBoard.score();

    // Break
    if (gameOver){
      return [null, score];
    }

    // max = [col, score]
    //!!!Still not fully sure what values are best to use here, so this is a placeholder
    var max = [null, -999];

    // Check for all possible moves
    for (var iterateColumn = 0; iterateColumn < col; iterateColumn++) {
        var new_gameBoard = updateGameBoard(row, col); // Create new board

        if (testBoard.place(iterateColumn)) {

            turn++;

            var nextMove = that.minimizeMove(testBoard, depth - 1, alpha, beta); 

            //evaluate the value of the new move proposed
            if (max[0] == null || nextMove[1] > max[1]) {
                max[0] = iterateColumn;
                max[1] = nextMove[1];
                alpha = nextMove[1];
            }

            if (alpha >= beta) return max;
        }
    }

    return max;
}

function minimizeMove(gameBoard, depth, alpha, beta) {
    var score = gameBoard.score();

    if (gameOver) return [null, score];

    // col, score
    var min = [null, 99999];

    for (var iterateColumn = 0; iterateColumn < col; iterateColumn++) {
        var testBoard = gameBoard.copy();

        if (testBoard.place(iterateColumn)) {

            turn++;

            var nextMove = that.maximizeMove(testBoard, depth - 1, alpha, beta);

            if (min[0] == null || nextMove[1] < min[1]) {
                min[0] = iterateColumn;
                min[1] = nextMove[1];
                beta = nextMove[1];
            }

            if (alpha >= beta) return min;
        }
    }
    return min;
}
