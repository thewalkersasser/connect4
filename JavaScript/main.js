
var turn = 0;
var gameBoard = [
                [0,0,0,0,0,0,0], //row 1
                [0,0,0,0,0,0,0], //row 2
                [0,0,0,0,0,0,0], //row 3
                [0,0,0,0,0,0,0], //row 4
                [0,0,0,0,0,0,0], //row 5
                [0,0,0,0,0,0,0]  //row 6
            ];
var gameOver = false;
// class gameBoard {

//     //Default constructor
//     constructor(gameOver) {
//         this.gameOver = false;
//         //Array
//         this.boardArray = [
//             [0,0,0,0,0,0,0], //row 1
//             [0,0,0,0,0,0,0], //row 2
//             [0,0,0,0,0,0,0], //row 3
//             [0,0,0,0,0,0,0], //row 4
//             [0,0,0,0,0,0,0], //row 5
//             [0,0,0,0,0,0,0]  //row 6
//         ];
//         this.score = 0;
//     }

//     //update the array representation of the game board
//     updateBoard(row, column) {
//         this.boardArray[row][column] = 1;
//     }

//     //return a score?
//     score() {
//         return this.score;
//     }

//     //I'm not sure this will work, but it should return a new gameBoard object that's essentially a copy of the original
//     copy() {
//         return gameBoard(false, this.boardArray, this.score)
//     }
// }


function checkGameBoard(){
    // call the check methods and return true if game over
    return checkRows(gameBoard) ||
        checkColumns(gameBoard);
}

function checkRows() {

   for (var row = 0; row < gameBoard.length; row++) {
       
       if (gameBoard[row].toString().includes("1,1,1,1") || gameBoard[row].toString().includes("-1,-1,-1,-1")) {
           return true;
       }
   }

   return false;
}

function checkColumns() {
    column = "";
    columns = "";
    for (var col = 0; col < gameBoard[0].length; col++) {
        for (var row = 0; row < gameBoard.length; row++) {
          
            column+=gameBoard[row][col];
        }

        columns+=" " + column;
        console.log(columns);
        column = "";
    }

    if (columns.includes("1111") || columns.includes("-1-1-1-1")) {
        return true;
    }
    return false;
}

function checkDiagonal1(){
   
}

function checkDiagonal2(){
    
}


//UPDATE the gameBoard array to reflect state of game
function updateGameBoard(row, col) {
    
    if (turn % 2 == 0)
        gameBoard[row][col] = 1; //positive 1 for x's (p1)
    else
        gameBoard[row][col] = -1; //negative 1 for o's (p2)
    
    gameOver = checkGameBoard(); //check if 4 in row somewhere
}

//PLACE TOKEN
document.getElementById('place').onclick = function(){
    var position = parseInt(document.getElementById('positionInput').value, 10) - 1;
    var table = document.getElementById("gameTable");
    var row = table.rows[6];
    var col = row.cells[position];
  
    for (var i = 5; i >= 0; i--) {
        row = table.rows[i];
        cell = row.cells[position].innerHTML;
    
        
        if (cell != 'X' && cell != 'O'){
            alert('hit a cell with no X at row ' + i + " col " + (position + 1));
            if (turn % 2 == 0) {
                row.cells[position].innerHTML = `<td id='cell'>X</td>`; 
                updateGameBoard(i, position);
            } else {
                row.cells[position].innerHTML = `<td id='cell'>O</td>`; 
                updateGameBoard(i, position);
            }
            turn++;
            break;
        } 
        
    }

    if (gameOver) {
        alert("Game over!");
    }

    
}



